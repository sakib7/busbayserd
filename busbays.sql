DROP DATABASE busbays;
CREATE DATABASE busbays;

CREATE TABLE driver (
    driver_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    contact_number VARCHAR(14) NOT NULL,
    PRIMARY KEY (driver_id)
) ENGINE=InnoDB;

CREATE TABLE supervisor (
    supervisor_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    contact_number VARCHAR(14) NOT NULL,
    PRIMARY KEY (supervisor_id)
) ENGINE=InnoDB;

CREATE TABLE district (
    district_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    division VARCHAR(255) NOT NULL,
    PRIMARY KEY (district_id)
) ENGINE=InnoDB;

CREATE TABLE bus_stop (
    bus_stop_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
	contact_number VARCHAR(255) NOT NULL,
	district_id INT NOT NULL,
    PRIMARY KEY (bus_stop_id),
	FOREIGN KEY (district_id) REFERENCES district(district_id)
) ENGINE=InnoDB;

CREATE TABLE restaurant (
    restaurant_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
	contact_number VARCHAR(255) NOT NULL,
	district_id INT NOT NULL,
    PRIMARY KEY (restaurant_id),
	FOREIGN KEY (district_id) REFERENCES district(district_id)
) ENGINE=InnoDB;

CREATE TABLE bus (
    bus_id INT NOT NULL AUTO_INCREMENT,
    model VARCHAR(255) NOT NULL,
    brand VARCHAR(255) NOT NULL,
	bus_type VARCHAR(255) NOT NULL,
	total_seat INT NOT NULL,
    PRIMARY KEY (bus_id)
) ENGINE=InnoDB;

CREATE TABLE route (
    route_id INT NOT NULL AUTO_INCREMENT,
    source_id INT NOT NULL,
    destination_id INT NOT NULL,
	bus_id INT NOT NULL, 
	restaurant_id INT NOT NULL,
	driver_id INT NOT NULL,
	supervisor_id INT NOT NULL,
	price INT NOT NULL,
    PRIMARY KEY (route_id),
	FOREIGN KEY (source_id) REFERENCES district(district_id),
	FOREIGN KEY (destination_id) REFERENCES district(district_id),
	FOREIGN KEY (bus_id) REFERENCES bus(bus_id),
	FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id),
	FOREIGN KEY (driver_id) REFERENCES driver(driver_id),
	FOREIGN KEY (supervisor_id) REFERENCES supervisor(supervisor_id)
) ENGINE=InnoDB;

CREATE TABLE route_bus_stop (
    route_bus_stop_id INT NOT NULL AUTO_INCREMENT,
    route_id INT NOT NULL,
	bus_stop_id INT NOT NULL,
    arrives_at TIME,
	position INT NOT NULL,
	distance INT NOT NULL,
    PRIMARY KEY (route_bus_stop_id),
	FOREIGN KEY (route_id) REFERENCES route(route_id),
	FOREIGN KEY (bus_stop_id) REFERENCES bus_stop(bus_stop_id)
) ENGINE=InnoDB;

CREATE TABLE passenger (
    passenger_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    contact_number VARCHAR(14) NOT NULL,
	luggage_weight INT NOT NULL,
    PRIMARY KEY (passenger_id)
) ENGINE=InnoDB;

CREATE TABLE booking_config (
    config_id INT NOT NULL AUTO_INCREMENT,
    duration TIME NOT NULL,
	activation_date DATE NOT NULL,
    PRIMARY KEY (config_id)
) ENGINE=InnoDB;

CREATE TABLE staff (
    staff_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    contact_number VARCHAR(14) NOT NULL,
	role VARCHAR(255) NOT NULL,
    PRIMARY KEY (staff_id)
) ENGINE=InnoDB;


CREATE TABLE ticket (
    ticket_id INT NOT NULL AUTO_INCREMENT,
    config_id INT NOT NULL,
    route_id INT NOT NULL,
	passenger_id INT NOT NULL,
	seat_no VARCHAR(255) NOT NULL,
	staff_id INT NOT NULL,
    PRIMARY KEY (ticket_id),
	FOREIGN KEY (config_id) REFERENCES booking_config(config_id),
	FOREIGN KEY (route_id) REFERENCES route(route_id),
	FOREIGN KEY (passenger_id) REFERENCES passenger(passenger_id),
	FOREIGN KEY (staff_id) REFERENCES staff(staff_id)
) ENGINE=InnoDB;